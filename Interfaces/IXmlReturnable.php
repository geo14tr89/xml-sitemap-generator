<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 20.12.2018
 * Time: 15:04
 */

namespace Interfaces;

interface IXmlReturnable
{
    public function getNode();
}