<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 03.01.2019
 * Time: 17:42
 */

namespace Interfaces;

interface IStoreService
{
    const START_OF_FILE = 'r+';
    const END_OF_FILE = 'a';

    public function __construct(
        $filename,
        $host,
        $port,
        $username,
        $password,
        $root,
        $mode = self::END_OF_FILE
    );

    public function store(IXmlReturnable $xmlUrlEntity);

    public function __destruct();
}