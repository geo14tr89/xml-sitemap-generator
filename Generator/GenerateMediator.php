<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 19.12.2018
 * Time: 20:14
 */

namespace Generator;

use Entities\CloseUrlSetXmlEntity;
use Entities\DocTypeXmlEntity;
use Entities\SftpConfigEntity;
use Entities\OpenUrlSetXmlEntity;
use Entities\UrlEntity;
use League\Flysystem\Sftp\SftpAdapter;
use Services\DatabaseService;
use Services\RemoteStorageService;
use Services\XmlService;
use tests\units\FtpClient\FtpClient;

class GenerateMediator
{
    const WORKING_TIME = 1800;

    private $databaseService;

    /**
     * @var RemoteStorageService
     */
    private $storeService;

    public function __construct()
    {
        $this->databaseService = new DatabaseService();
    }

    public function init()
    {
        echo 'start' . PHP_EOL;

        foreach ($this->databaseService->getProjectConfig() as $project) {
            $startTime = microtime(true);
            if ($this->databaseService->checkUrlStatus($project->tableName)) {
                $this->deleteXmlFileIfExists($project->tableName . '_sitemap.xml');
                $this->storeService = new RemoteStorageService(
                    $project->tableName . '_sitemap.xml',
                    $project->host,
                    $project->port,
                    $project->username,
                    $project->password,
                    $project->root,
                    $mode = RemoteStorageService::END_OF_FILE
                );
                echo 'Working with ' . $project->tableName . '_sitemap.xml' . PHP_EOL;
                $this->addOpenXmlFileTags();

                while (($urlEntity = $this->databaseService->getUrlEntity($project->tableName)) &&
                    (microtime(true) < $startTime + self::WORKING_TIME)) {
                    $xmlService = new XmlService($urlEntity);
                    $xmlEntity = $xmlService->getXmlEntity();
                    $this->storeService->store($xmlEntity);
                    $this->databaseService->updateUrlStatus(
                        $urlEntity,
                        $project->tableName,
                        UrlEntity::STATUS_EXPORTED
                    );
                }
                $this->addCloseXmlFileTags();
                $this->databaseService->updateUrlStatusForTask($project->id, SftpConfigEntity::PROJECT_STATUS_FINISH);
            } else {
                echo 'Sitemap ' . $project->tableName . '_sitemap.xml is ready!' . PHP_EOL;
            }
            $xmlContent = $this->storeService->getXmlContent($project->tableName . '_sitemap.xml');
            $this->storeService->copyFile(
                $project->tableName . '_sitemap.xml',
                $xmlContent
            );
        }
        echo 'finish' . PHP_EOL;
    }

    private function addOpenXmlFileTags()
    {
        $this->storeService->store(new DocTypeXmlEntity());
        $this->storeService->store(new OpenUrlSetXmlEntity());
    }

    private function addCloseXmlFileTags()
    {
        $this->storeService->store(new CloseUrlSetXmlEntity());
    }

    private function deleteXmlFileIfExists($file)
    {
        return is_file($file) ? unlink($file) : false;
    }
}
