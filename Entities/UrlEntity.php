<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 19.12.2018
 * Time: 19:59
 */

namespace Entities;

class UrlEntity
{
    const STATUS_NEW = 'new';
    const STATUS_WORKING = 'working';
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';
    const STATUS_EXPORTING = 'exporting';
    const STATUS_EXPORTED = 'exported';

    public $url;
    public $id;
    public $status;

    public function __construct($url, $id = null, $status = self::STATUS_SUCCESS)
    {
        $this->url = $url;
        $this->id = $id;
        $this->status = $status;
    }
}
