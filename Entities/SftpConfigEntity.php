<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 28.12.2018
 * Time: 14:43
 */

namespace Entities;

class SftpConfigEntity
{
    const PROJECT_STATUS_SUCCESS = 'success';
    const PROJECT_STATUS_READY_TO_GENERATE = 'ready_to_generate';
    const PROJECT_STATUS_FINISH = 'finish';

    public $id;
    public $status;
    public $host;
    public $port;
    public $username;
    public $password;
    public $root;
    public $tableName;

    public function __construct($id, $status, $host, $port, $username, $password, $root, $tableName)
    {
        $this->id = $id;
        $this->status = $status;
        $this->host = $host;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;
        $this->root = $root;
        $this->tableName = $tableName;
    }
}
