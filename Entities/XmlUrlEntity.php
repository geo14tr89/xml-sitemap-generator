<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 20.12.2018
 * Time: 15:03
 */

namespace Entities;


use Interfaces\IXmlReturnable;

class XmlUrlEntity implements IXmlReturnable
{
    protected $loc;
    protected $lastMod;
    protected $changeFreq;
    protected $priority;

    public function __construct($loc, $lastMod, $changeFreq, $priority)
    {
        $this->loc = $loc;
        $this->lastMod = $lastMod;
        $this->changeFreq = $changeFreq;
        $this->priority = $priority;
    }

    public function getNode()
    {
        if (!empty($this->loc) && !empty($this->lastMod) && !empty($this->priority)) {
            $xmlLoc = '<loc>' . $this->loc . '</loc>';
            $xmlLastMod = '<lastmod>' . $this->lastMod . '</lastmod>';
            $changeFreq = '<changefreq>' . $this->changeFreq . '</changefreq>';
            $xmlPriority = '<priority>' . $this->priority . '</priority>';
            return '<url>' . $xmlLoc . $xmlLastMod . $changeFreq . $xmlPriority . '</url>';
        }
        return false;
    }
}