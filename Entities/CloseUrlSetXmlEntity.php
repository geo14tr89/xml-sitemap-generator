<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 27.12.2018
 * Time: 18:05
 */

namespace Entities;

use Interfaces\AAdditionalXmlEntity;

class CloseUrlSetXmlEntity extends AAdditionalXmlEntity
{

    public function getNode()
    {
        return '</urlset>';
    }
}