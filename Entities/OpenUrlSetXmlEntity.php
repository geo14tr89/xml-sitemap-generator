<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 27.12.2018
 * Time: 18:04
 */

namespace Entities;

use Interfaces\AAdditionalXmlEntity;

class OpenUrlSetXmlEntity extends AAdditionalXmlEntity
{
    public function getNode()
    {
        return '<urlset
            xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
    }
}
