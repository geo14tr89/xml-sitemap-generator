<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 27.12.2018
 * Time: 18:02
 */

namespace Entities;


use Interfaces\AAdditionalXmlEntity;

class DocTypeXmlEntity extends AAdditionalXmlEntity
{

    public function getNode()
    {
        return '<?xml version="1.0" encoding="UTF-8"?>';
    }
}