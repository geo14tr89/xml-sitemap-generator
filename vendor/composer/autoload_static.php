<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitf0be86323c9584d808004ffca700952c
{
    public static $files = array (
        'decc78cc4436b1292c6c0d151b19445c' => __DIR__ . '/..' . '/phpseclib/phpseclib/phpseclib/bootstrap.php',
    );

    public static $prefixLengthsPsr4 = array (
        'p' => 
        array (
            'phpseclib\\' => 10,
        ),
        'S' => 
        array (
            'Services\\' => 9,
        ),
        'L' => 
        array (
            'League\\Flysystem\\Sftp\\' => 22,
            'League\\Flysystem\\' => 17,
        ),
        'I' => 
        array (
            'Interfaces\\' => 11,
        ),
        'G' => 
        array (
            'Generator\\' => 10,
        ),
        'E' => 
        array (
            'Exceptions\\' => 11,
            'Entities\\' => 9,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'phpseclib\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpseclib/phpseclib/phpseclib',
        ),
        'Services\\' => 
        array (
            0 => __DIR__ . '/../..' . '/Services',
        ),
        'League\\Flysystem\\Sftp\\' => 
        array (
            0 => __DIR__ . '/..' . '/league/flysystem-sftp/src',
        ),
        'League\\Flysystem\\' => 
        array (
            0 => __DIR__ . '/..' . '/league/flysystem/src',
        ),
        'Interfaces\\' => 
        array (
            0 => __DIR__ . '/../..' . '/Interfaces',
        ),
        'Generator\\' => 
        array (
            0 => __DIR__ . '/../..' . '/Generator',
        ),
        'Exceptions\\' => 
        array (
            0 => __DIR__ . '/../..' . '/Exceptions',
        ),
        'Entities\\' => 
        array (
            0 => __DIR__ . '/../..' . '/Entities',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitf0be86323c9584d808004ffca700952c::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitf0be86323c9584d808004ffca700952c::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
