<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 19.12.2018
 * Time: 20:02
 */

namespace Services;

use Entities\SftpConfigEntity;
use Entities\UrlEntity;
use PDO;

class DatabaseService
{
    private $pdo;

    public function __construct()
    {
        $host = 'localhost';
        $db   = 'generator';
        $user = 'root';
        $pass = '';
        $charset = 'utf8';

        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $this->pdo = new PDO($dsn, $user, $pass, $opt);
    }

    public function getProjectConfig()
    {
        $sftpConfigEntityArray = [];
        $query = $this->pdo->query("SELECT * FROM parse_sitemap_tasks WHERE status = 'success'");
        while ($config = $query->fetch()) {
            $sftpConfigEntityArray[] = new SftpConfigEntity(
                $config['id'],
                $config['status'],
                $config['host'],
                $config['port'],
                $config['username'],
                $config['password'],
                $config['root'],
                $config['table_name_for_urls']
            );
        }
        return ($config === null) ? false : $sftpConfigEntityArray;
    }

    public function getUrlEntity($table)
    {
        $url = $this->pdo->query("SELECT * FROM " . $table . " WHERE status = 'success' LIMIT 1")->fetch();
        $urlEntity = new UrlEntity($url['url'], $url['id'], $url['status']);
        $this->updateUrlStatus($urlEntity, $table, UrlEntity::STATUS_EXPORTING);
        if ($url['url'] === null) {
            return false;
        } else {
            return $urlEntity;
        }
    }

    public function checkUrlStatus($table)
    {
        $status = $this->pdo->query("SELECT status FROM " . $table . " WHERE status = 'success' LIMIT 1")->fetch();
        return ($status['status'] === 'success');
    }

    public function updateUrlStatus(UrlEntity $urlEntity, $table, $status)
    {
        $this->pdo->exec("UPDATE " . $table . " SET status = '" . $status . "' WHERE id = '" . $urlEntity->id . "'");
    }

    public function updateUrlStatusForTask($id, $status)
    {
        $this->pdo->exec(
            "UPDATE parse_sitemap_tasks SET status = '" . $status . "' WHERE id = '" . $id . "'"
        );
    }
}
