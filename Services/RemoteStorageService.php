<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 28.12.2018
 * Time: 14:40
 */

namespace Services;

use Entities\SftpConfigEntity;
use Interfaces\IStoreService;
use Interfaces\IXmlReturnable;
use League\Flysystem\AdapterInterface;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\Filesystem;
use League\Flysystem\Sftp\SftpAdapter;

class RemoteStorageService implements IStoreService
{
    const DEFAULT_TIMEOUT = 10;

    public $fp;
    public $fileName;
    public $host;
    public $port;
    public $username;
    public $password;
    public $root;
    public $fileSystem;

    public function __construct(
        $fileName,
        $host,
        $port,
        $username,
        $password,
        $root,
        $mode = IStoreService::END_OF_FILE
    ) {
        $this->fileName = $fileName;
        $this->fp = fopen($this->fileName, $mode);

        $this->host = $host;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;
        $this->root = $root;

        $adapter = new SftpAdapter([
            'host' => $this->host,
            'port' => $this->port,
            'username' => $this->username,
            'password' => $this->password,
            'root' => $this->root,
            'timeout' => self::DEFAULT_TIMEOUT,
        ]);
        $this->fileSystem = new Filesystem($adapter);
    }

    public function store(IXmlReturnable $xmlUrlEntity)
    {
        fwrite(
            $this->fp,
            $xmlUrlEntity->getNode() . PHP_EOL
        );
    }

    public function copyFile($file, $content)
    {
        return $this->fileSystem->put($file, $content);
    }

    public function getXmlContent($file)
    {
        return file_get_contents($file);
    }

    public function __destruct()
    {
        fclose($this->fp);
    }
}
