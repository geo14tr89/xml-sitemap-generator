<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 20.12.2018
 * Time: 15:03
 */

namespace Services;


use Entities\UrlEntity;
use Entities\XmlUrlEntity;

class XmlService
{
    const DEFAULT_CHANGE_FREQ = 'weekly';
    const DEFAULT_PRIORITY = 0.67;

    protected $urlEntity;
    protected $xmlEntity;

    public function __construct(UrlEntity $url)
    {
        $this->urlEntity = $url;
        $this->xmlEntity = new XmlUrlEntity(
            $this->getLoc(),
            $this->getLastMod(),
            $this->getChangeFreq(),
            $this->getPriority()
        );
    }

    public function getLoc()
    {
        return $this->urlEntity->url;
    }

    public function getChangeFreq()
    {
        return self::DEFAULT_CHANGE_FREQ;
    }

    public function getLastMod()
    {
        return date('c');
    }

    public function getPriority()
    {
        return self::DEFAULT_PRIORITY;
    }

    public function getResult()
    {
        return $this->urlEntity;
    }

    public function getXmlEntity()
    {
        return $this->xmlEntity;
    }
}